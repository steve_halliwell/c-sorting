#ifndef ABSORTEST_H
#define ABSORTEST_H

#include <string>
#include <vector>
#include <sstream>
#include <stdint.h>

class ABSortTest
{
public:
	ABSortTest(const std::string& name, std::vector<int>& data):
		mData(data),
		mName(name)
	{
	}

	void Go()
	{
		RunTest();
	}

	
	std::string GetResultString()
	{
		std::stringstream ss;
		std::string spacer = "";

		ss << mName << '\n';

		if (showSortedList)
		{
			for (size_t i = 0, size = mData.size(); i < size; i++)
			{
				ss << mData[i] << ',';
			}
			ss << "\n\n";
		}
		else
			spacer = '\t';


		ss << spacer << "ifs: " << mNumIfs << '\n';
		ss << spacer << "compares: " << mNumCompares << '\n';
		ss << spacer << "copies: " << mNumCopies << '\n';
		ss << spacer << "swaps: " << mNumSwaps << '\n';
		ss << spacer << "increments: " << mNumIncrements << '\n';
		ss << spacer << "data overhead ratio: " << mAdditionData << '\n';

		return ss.str();
	}

protected:

	void Swap(std::vector<int>& vec, size_t lhs, size_t rhs)
	{
		mNumSwaps++;
		int tmp = vec[lhs];
		vec[lhs] = vec[rhs];
		vec[rhs] = tmp;
	}

	virtual void RunTest() = 0;

	uint64_t mNumIfs = 0;			//how many ifs are required by the alg
	uint64_t mNumCompares = 0;		//how many compares are done <> etc.
	uint64_t mNumCopies = 0;		//how many data copies are required
	uint64_t mNumSwaps = 0;			//how many data swaps (can think of this as 3 copies if you like)
	uint64_t mNumIncrements = 0;	//how many increments 
	double mAdditionData = 0;		//ratio of how much extra data did we have to alloc to run the alg

	bool showSortedList = false;

	std::string mName;
	std::vector<int> mData;

private:
};

#endif//ABSORTEST_H