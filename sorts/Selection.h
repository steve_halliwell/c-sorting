

//selection moves find the next largest or smallest in all remaining unsorted data and moves to to the appropriate end
class SelectionSort : public ABSortTest
{
public:
	SelectionSort(const std::string& name, std::vector<int>& data) : ABSortTest(name, data){}

protected:
	virtual void RunTest()
	{
		//for all data
		for (size_t i = 0, size = mData.size(); i < size; i++)
		{
			++mNumCompares;
			++mNumIfs;
			++mNumIncrements;

			int max = 0;

			//for all unlocked data
			for (size_t j = 1; j < mData.size() - i; j++)
			{
				++mNumCompares;
				++mNumIfs;
				++mNumIncrements;

				if (mData[max] < mData[j])
				{
					max = j;
				}
				++mNumCompares;
				++mNumIfs;
			}

			//swap
			Swap(mData, max, mData.size() - i - 1);
		}
	}
};