//builds a binary heap of the data and then converts that to a sorted list
//	Note this can be done inplace by reverse sorting the reverse filling the array
class HeapSort : public ABSortTest
{
public:
	HeapSort(const std::string& name, std::vector<int>& data) : ABSortTest(name, data){}

protected:
	virtual void RunTest()
	{
		mAdditionData = 1;

		//create heap
		std::vector<int> heap;
		heap.reserve(mData.size() + 1);
		heap.push_back(0);//dumby 0 node makes the math easier to read

		//for all data, add to back and bubble up thro heap
		for (size_t i = 0; i < mData.size(); i++)
		{
			++mNumCompares;
			++mNumIfs;
			++mNumIncrements;
			heap.push_back(mData[i]);
			++mNumCopies;

			BubbleUp(heap);
		}

		//for all data take top

		for (size_t i = 0; i < mData.size(); i++)
		{
			++mNumCompares;
			++mNumIfs;
			++mNumIncrements;
			mData[i] = heap[1];
			++mNumCopies;

			//remember fake 0 node for math working out
			heap[1] = heap.back();
			++mNumCopies;
			heap.pop_back();


			TrickleDown(heap);
		}

	}

	void BubbleUp(std::vector<int>& heap)
	{
		size_t p = 0, index = heap.size() - 1;
		while (index > 1)
		{
			++mNumCompares;
			++mNumIfs;

			p = ParentIndex(index);
			if (heap[index] < heap[p])
			{
				++mNumCompares;
				++mNumIfs;

				Swap(heap, index, p);
				index = p;
			}
			else
				break;
		}
	}


	void TrickleDown(std::vector<int>& heap)
	{
		size_t index = 1;
		while (true)
		{
			++mNumIfs;
			//is it valid to trickle
			size_t leftChild = index * 2;
			size_t rightChild = leftChild + 1;

			//if right is valid so is left, if right is the smaller then use it
			if (rightChild < heap.size() && heap[index] > heap[rightChild] && heap[leftChild] > heap[rightChild])
			{
				mNumCompares += 3;
				++mNumIfs;
				Swap(heap, index, rightChild);
				index = rightChild;
			}
			else if (leftChild < heap.size() && heap[index] > heap[leftChild])
			{
				mNumCompares += 5;
				++mNumIfs;
				Swap(heap, index, leftChild);
				index = leftChild;
			}
			else
			{
				mNumCompares += 5;
				++mNumIfs;
				break;
			}
		}
	}

	size_t ParentIndex(size_t index)
	{
		return index / 2;
	}
};