
//is a bubble style sort that incrementally introduces more data into the already sorted collection
class InsertionSort : public ABSortTest
{
public:
	InsertionSort(const std::string& name, std::vector<int>& data) : ABSortTest(name, data){}

protected:
	virtual void RunTest()
	{
		//for all data
		for (size_t i = 0, size = mData.size(); i < size; i++)
		{
			++mNumCompares;
			++mNumIfs;
			++mNumIncrements;

			bool swapped = true;

			//for all unlocked data
			for (size_t j = i; j > 0 && swapped; j--)
			{
				mNumCompares += 2;
				++mNumIfs;
				++mNumIncrements;

				if (mData[j] < mData[j - 1])
				{
					Swap(mData, j, j - 1);
				}
				else
				{
					swapped = false;
				}
				++mNumCompares;
				++mNumIfs;
			}
		}
	}
};