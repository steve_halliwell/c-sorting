
//
class BottomUpMergeSort : public ABSortTest
{
public:
	BottomUpMergeSort(const std::string& name, std::vector<int>& data) : ABSortTest(name, data){}

	std::vector<int> bDat;
	std::vector<int> *a, *b;
protected:
	virtual void RunTest()
	{
		bDat = mData;
		mAdditionData = 1;
		a = &mData;
		b = &bDat;
		//for ever expanding increments of 2
		for (size_t width = 1, size = mData.size(); width < size; width *= 2)
		{
			++mNumCompares;
			++mNumIfs;
			++mNumIncrements;

			InternalIteration(width, size);

			//swap a and b
			std::vector<int>*tmp = a;
			a = b;
			b = tmp;
			++mNumSwaps;
		}

		mData = *a;
	}

private:
	void InternalIteration(size_t width, size_t size)
	{
		//for widths
		for (size_t i = 0; i < size; i += width * 2)
		{
			++mNumCompares;
			++mNumIfs;
			++mNumIncrements;

			size_t lhs = i;
			size_t rhs = i + width;
			mNumCopies += 2;

			//for left and right side lists, merge
			for (size_t j = 0, len = width * 2; j < len && i + j < size; j++)
			{
				mNumCompares += 2;
				++mNumIfs;
				++mNumIncrements;



				//merge the lists

				//if they are both empty bail out
				bool leftEmpty = lhs >= i + width;
				bool rightEmpty = (rhs >= i + 2 * width || rhs >= size);
				mNumCompares += 3;

				if (leftEmpty && rightEmpty)
				{
					++mNumIfs;
					break;
				}
				else if (leftEmpty/* but not right*/)
				{
					mNumIfs += 2;
					(*b)[i + j] = (*a)[rhs];
					++mNumCopies;
					++rhs;
					++mNumIncrements;
				}
				else if (rightEmpty/* but not left*/)
				{
					mNumIfs += 3;
					(*b)[i + j] = (*a)[lhs];
					++mNumCopies;
					++lhs;
					++mNumIncrements;
				}
				else if ((*a)[lhs] < (*a)[rhs])
				{
					mNumIfs += 4;
					(*b)[i + j] = (*a)[lhs];
					++mNumCopies;
					++lhs;
					++mNumIncrements;
				}
				else
				{
					mNumIfs += 5;
					(*b)[i + j] = (*a)[rhs];
					++mNumCopies;
					++rhs;
					++mNumIncrements;
				}
			}
		}
	}
};
