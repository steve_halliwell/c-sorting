
#include <stack>

//quick sort uses a pivot to flip the data either side of it. 
//	It originally recursed, unfortunately this makes it kinda unusable for large data sets
//	so now we use an explicit stack
class QuickSort : public ABSortTest
{
public:
	QuickSort(const std::string& name, std::vector<int>& data) : ABSortTest(name, data){}

protected:
	virtual void RunTest()
	{
		quick_sort(0, mData.size());
	}
	//orig
	//	void quick_sort(size_t start, size_t len) {
	//	++mNumIfs;
	//	++mNumCompares;
	//	if (len < 2)
	//		return;

	//	size_t lower = start, upper = start + len - 1;

	//	//choosing 'random' half way pivot
	//	int p = mData[lower + len / 2];
	//	++mNumCopies;

	//	//bring the arms in, when we find values that are on the wrong side of the pivot we switch them
	//	while (lower <= upper) {
	//		++mNumCompares;
	//		++mNumIfs;
	//		if (mData[lower] < p) {
	//			++mNumCompares;
	//			++mNumIfs;
	//			++lower;
	//			++mNumIncrements;
	//			continue;
	//		}
	//		if (mData[upper] > p) {
	//			++mNumCompares;
	//			++mNumIfs;
	//			--upper;
	//			++mNumIncrements;
	//			continue;
	//		}

	//		Swap(mData, lower, upper);
	//		++lower;
	//		++mNumIncrements;
	//		--upper;
	//		++mNumIncrements;
	//	}


	//	//std::cout << start << ',' << lower << ',' << upper << ',' << start + len << '\n';
	//	//we've flipped all of them around this pivot, choose some new segments
	//	quick_sort(start, upper - start + 1);	//from our starting point to the last upper we messed with
	//	quick_sort(lower, start + len - lower);	//from the last lower we messed with to the end
	//}

	
	void quick_sort(size_t startingPoint, size_t length) {

		std::stack<std::pair<size_t, size_t>> indexLenStack;

		indexLenStack.push(std::make_pair(startingPoint, startingPoint + length-1));

		while (!indexLenStack.empty())
		{
			++mNumIfs;
			++mNumCompares;
			auto cur = indexLenStack.top();
			size_t lower = cur.first, upper = cur.second;
			indexLenStack.pop();

			size_t segLength = upper - lower + 1;

			++mNumIfs;
			++mNumCompares;
			if (segLength < 2)
				continue;
			
			//choosing 'random' half way pivot
			int p = mData[lower + segLength / 2];
			++mNumCopies;

			//bring the arms in, when we find values that are on the wrong side of the pivot we switch them
			while (lower <= upper) {
				++mNumCompares;
				++mNumIfs;
				if (mData[lower] < p) {
					++mNumCompares;
					++mNumIfs;
					++lower;
					++mNumIncrements;
					continue;
				}
				if (mData[upper] > p) {
					++mNumCompares;
					++mNumIfs;
					--upper;
					++mNumIncrements;
					continue;
				}

				Swap(mData, lower, upper);
				++lower;
				++mNumIncrements;
				--upper;
				++mNumIncrements;
			}
		//	std::cout << cur.first << ',' << lower << ',' << upper << ',' << cur.second << '\n';
			//we've flipped all of them around this pivot, choose some new segments
			indexLenStack.push(std::make_pair(lower, cur.second));	//from the last lower we messed with to the end
			indexLenStack.push(std::make_pair(cur.first, upper));	//from our starting point to the last upper we messed with
		}
	}
	
};
