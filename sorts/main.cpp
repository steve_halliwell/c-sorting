#include <iostream>
#include "ABSortTest.h"
#include <cmath>
#include <time.h>

#include "Insertion.h"
#include "Merge.h"
#include "Selection.h"
#include "Quick.h"
#include "Heap.h"



void main()
{
	std::vector<int> data;
	int numData = 125;
	data.reserve(numData);

	srand(time(nullptr));

	for (int i = 0; i < numData; i++)
	{
		data.push_back(rand());
	}

	SelectionSort selection("SelectionSort", data);
	selection.Go();

	InsertionSort insert("InsertionSort", data);
	insert.Go();

	BottomUpMergeSort merge("BottomUpMergeSort", data);
	merge.Go();

	QuickSort quick("QuickSort", data);
	quick.Go();

	HeapSort heap("HeapSort", data);
	heap.Go();

	std::cout << selection.GetResultString() << std::endl;
	std::cout << insert.GetResultString() << std::endl;
	std::cout << merge.GetResultString() << std::endl;
	std::cout << quick.GetResultString() << std::endl;
	std::cout << heap.GetResultString() << std::endl;
}